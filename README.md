# NHS Application Suite - Developer Manual

Welcome! If you're reading this, you're the developer/maintainer of the NHS Application suite, trusted by several hundred prospective members of the National Honor Society every January.

This manual shall contain *everything* you will need to:

* understand the code
* write new code
* maintain and orchestrate critical infrastructure

## Codebase

A **codebase** refers to a collection of *source code*. **Source code** is just a fancy word for code.

We have 5 codebases:

* [applicant-site](https://gitlab.com/nbnhs/application/applicant-site)
* [faculty-site](https://gitlab.com/nbnhs/application/faculty-site)
* [admin-site](https://gitlab.com/nbnhs/application/admin-site)
* [database-relay](https://gitlab.com/nbnhs/application/database-relay)
* [applicant-populator](https://gitlab.com/nbnhs/application/applicant-populator)

The `-site` codebases constitute the three websites:

* the applicant-facing website (where all portions of the application except the teacher recommendation are submitted),
* the faculty-facing site (where the teacher recommendation is submitted), and
* the admin site (where NHS officers can review completed applications)

`database-relay` contains the *API* for the whole suite (see the section on the relay for more information), and `applicant-populator` serves to fill up the database with Applicant logins when given a CSV of all the applicants.

## Relay and Infrastructure

To understand how these sites work, you must first understand the infrastructure which it's built upon.

The `database-relay` codebase contains an API (short for **Application Programming Interface**) and is used by the websites to access remote information. An API is just a program that can get, set, and delete information in the back-end of a service.

So what's "remote information?" It's the individual parts of the application as completed by the applicant. This information is "remote" because it's stored on remote servers with service providers such as Amazon S3 and Google Cloud Datastore.

**Amazon S3** is like Google Drive for programmers. It provides an easy-to-use API for uploading and retrieving files, such as image files. Given that our application site asks for all parts except for Leadership to be uploaded as images, ease-of-use on the programmatic side is very important.

**Google Cloud Datastore** is a database. It contains *entities* of various *kinds*. In our case, we have kinds called "Applicant", "Faculty", and "Admin". Entities belonging to these various kinds have certain *properties*.

### Applicant Properties

An applicant has 6 properties: first and last names, leadership, service, signature, and teacher.

Excluding `first_name`, `last_name`, and `leadership`, all the properties of an Applicant contain the path to an image in Amazon S3. For example, `teacher` would contain `999999/teacher.jpeg` if a teacher uploaded their recommendation form as a JPEG for the student with ID #999999.

`leadership` contains a *JSON*-formatted *string*. JSON, a.k.a. **JavaScript Object Notation**, is a way of compressing programmatic information into easily-interpretable chunks (easy for a computer to interpret, that is). A **string** is simply a sequence of characters (e.g. "a" is a character; "ahem" is a string). The format for this JSON looks something like:

```javascript
[
  {
    "yearsInvolved": ["9th"],
    "description": "Blah blah."
  },
  {
    "yearsInvolved": ["9th", "10th"],
    "description": "Et cetera."
  },
  {
    "yearsInvolved": ["10th"],
    "description": "Text goes here."
  }
]
```

`yearsInvolved` contains the years the person was involved for a certain activity, and `description` is just a string containing the activity's description. This is repeated 3 times, once for each activity.

### Faculty & Admin Properties

Both Faculty and Admin entities have the same property: `name`, a string containing the name of the person the entity represents.

The kicker comes from the ID numbers of these entities. Unlike student ID numbers, they are randomly generated and disclosed to the people they represent via email, so they can be used as unique log-in numbers (the trick being only Faculty and Admin members have these numbers, so Applicants can't, say, upload their own teacher recommendation or see completed applications).

## Program

The relay itself is what's called a *RESTful API*. **RESTful APIs** use HTTP to conduct their business.

For example, downloading the url `https://relay.nbnhs.club?operation=get&id=someID` would yield information about the Applicant corresponding to `someID` in the Google Cloud Datastore.

That request is an example of a **GET** request; used to query and receive information. Our relay can also handle **POST** requests, which are used to update/upload information.

## Sites

The three websites are built on *TypeScript*, a superset of *JavaScript*, and are compiled with *webpack*.

* **JavaScript** is a programming language specifically designed to work in Internet browsers. It can change the content in HTML, respond to user interaction, and much more.
* **TypeScript** adds *type safety* to JavaScript. **Type safety** forces a program to know exactly what kind of data it is dealing with before it deals with it. Putting type safety in our website enables us to better control how users interact with it.

